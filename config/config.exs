# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :karavan,
  ecto_repos: [Karavan.Repo]

# Configures the endpoint
config :karavan, KaravanWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "v2G6o0J/wI+HeYrDrCiAHI+F6tPSSNyDeoaB3R69TfUFmVvOOg5gs6y62VVWnVRx",
  render_errors: [view: KaravanWeb.ErrorView, accepts: ~w(html json)]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :karavan, :instance,
       domain_name: "localhost",
       email_from: "noreply@localhost",
       registrations_opened: true

config :bamboo, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
