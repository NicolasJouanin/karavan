defmodule Karavan.Repo do
  use Ecto.Repo,
    otp_app: :karavan,
    adapter: Ecto.Adapters.Postgres
end
