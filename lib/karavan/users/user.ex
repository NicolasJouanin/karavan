defmodule Karavan.Users.User do
  import Ecto.Changeset
  use Ecto.Schema
  alias Karavan.Users.User

  @email_regex ~r<(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])>

  schema "users" do
    field :email, :string
    field :password, :string, virtual: true
    field :password_hash, :string
    field :confirmed_at, :utc_datetime
    field :confirmation_sent_at, :utc_datetime
    field :confirmation_token, :string
    timestamps()

  end

  @doc false
  def changeset(%User{} = user, attrs \\ %{}) do
      user
      |> cast(attrs, [
        :email,
        :password,
        :password_hash,
        :confirmed_at,
        :confirmation_sent_at,
        :confirmation_token
      ])
  end


  @doc false
  def create_changeset(%User{} = user, params) do
    user
    |> changeset(params)
    |> validate_required([:email])
    |> validate_format(:email, @email_regex)
    |> unique_constraint(:email)
    |> validate_length(:password, min: 6, max: 100)
    |> validate_required([:password], message: "user.changeset.missing.password")
    |> hash_password
  end

  defp hash_password(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: password}} ->
        change(changeset, Argon2.add_hash(password))
      _ ->
        changeset
    end
  end
end
