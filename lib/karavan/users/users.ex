defmodule Karavan.Users do
  alias Karavan.Users.User

  @doc """
  Create a new user
  """
  @spec create(map()) :: {:ok, User.t()} | {:error, String.t()}
  def create(%{email: _email, password: _password} = args) do
     %User{} |> User.create_changeset(args) |> Karavan.Repo.insert()
  end


  @spec update_user(User.t(), map()) :: {:ok, User.t()} | {:error, String.t()}
  def update_user(%User{} = user, attrs) do
    user |> User.changeset(attrs) |> Karavan.Repo.update()
  end

end
