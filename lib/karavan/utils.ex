defmodule Karavan.Utils do
  @spec random_string(non_neg_integer()) :: binary()
  def random_string(length \\ 30) do
    length |> :crypto.strong_rand_bytes() |> Base.url_encode64(padding: false)
  end
end
