defmodule KaravanWeb.PageController do
  use KaravanWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
