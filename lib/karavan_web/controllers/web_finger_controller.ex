defmodule KaravanWeb.WebFingerController do
  use KaravanWeb, :controller

  def host_meta(conn, _params) do
    base_url = KaravanWeb.Endpoint.url()

    conn
    |> put_resp_content_type("application/xrd+xml")
    |> text("""
    <?xml version="1.0" encoding="UTF-8"?>
    <XRD xmlns="http://docs.oasis-open.org/ns/xri/xrd-1.0">
      <hm:Host>@domainName</hm:Host>
      <Link rel="lrdd" type="application/xrd+xml" template="#{base_url}/.well-known/webfinger?resource={uri}"/>
    </XRD>
    """)
  end

end