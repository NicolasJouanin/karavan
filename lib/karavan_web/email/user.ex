defmodule KaravanWeb.Email.User do
  import Bamboo.Email
  import Bamboo.Phoenix
  use Bamboo.Phoenix, view: KaravanWeb.EmailView
  import KaravanWeb.Gettext

  alias Karavan.Users.User

  @instance Application.get_env(:karavan, :instance)
  @domain_name Keyword.get(@instance, :domain_name)
  @email_from Keyword.get(@instance, :email_from)
  @reply_to Keyword.get(@instance, :email_from)


  def registration_email(%User{} = user, confirmation_token, locale \\ "en") do
    Gettext.put_locale(locale)
    base_email()
    |> to(user.email)
    |> subject(
         gettext("Karavan: Confirmation instructions for %{domain_name}", domain_name: @domain_name)
       )
    |> put_header("Reply-To", @reply_to)
    |> assign(:token, confirmation_token)
    |> assign(:domain_name, @domain_name)
    |> render(:registration_confirmation)
  end

  defp base_email do
    # Here you can set a default from, default headers, etc.
    new_email()
    |> from(@email_from)
    |> put_html_layout({KaravanWeb.LayoutView, "email.html"})
    |> put_text_layout({KaravanWeb.LayoutView, "email.text"})
  end
end
