defmodule KaravanWeb.Resolvers.User do

  @instance Application.get_env(:karavan, :instance)
  @domain_name Keyword.get(@instance, :domain_name)

  @doc """
  Register a user.

  Register and return the new user
  """
  def register_user(_parent, request = %{email: _email, password: _password}, _resolution) do
    with {:ok, user} <- Karavan.Users.create(request),
      confirmation_token <- Karavan.Utils.random_string(),
      _ <- KaravanWeb.Email.User.registration_email(user, confirmation_token) |> Karavan.Mailer.deliver_later,
      user <- Karavan.Users.update_user(user, %{confirmation_sent_at: DateTime.utc_now(), confirmation_token: confirmation_token}) do
      user
    else
      {:error, %Ecto.Changeset{} = changeset} -> {:ok, changeset}
    end
  end
end
