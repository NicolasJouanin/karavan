defmodule KaravanWeb.Router do
  use KaravanWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :well_known do
    plug(:accepts, ["json", "jrd+json"])
  end

  scope "/" do
    pipe_through :browser

  end

  scope "/.well-known", KaravanWeb do
    pipe_through(:well_known)

    get "/host-meta", WebFingerController, :host_meta
    #get"/webfinger", WebFingerController, :webfinger
  end

  scope "/", KaravanWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  scope "/api" do
    pipe_through(:api)

    forward "/", Absinthe.Plug, schema: KaravanWeb.Schema, json_codec: Jason
  end

  if Mix.env == :dev do
    forward "/graphiql", Absinthe.Plug.GraphiQL, schema: KaravanWeb.Schema, json_codec: Jason
    # If using Phoenix
    forward "/emails", Bamboo.SentEmailViewerPlug
  end
end
