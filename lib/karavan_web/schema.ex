defmodule KaravanWeb.Schema do
  use Absinthe.Schema
  import_types KaravanWeb.Schema.UserType

  alias KaravanWeb.Resolvers

  query do

  end

  mutation do
    import_fields :user_mutations
  end

end
