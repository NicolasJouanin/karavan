defmodule KaravanWeb.Schema.UserType do
  use Absinthe.Schema.Notation
  import AbsintheErrorPayload.Payload
  import_types AbsintheErrorPayload.ValidationMessageTypes

  object :user do
    field :email, non_null(:string), description: "user email address"
    field :confirmation_token, :string, description: "registration confirmation token"
  end

  payload_object(:user_payload, :user)

  object :user_mutations do
    @desc "Register a new user"
    field :register_user, type: :user_payload, description: "Register a new user" do
      arg :email, non_null(:string), description: "User email"
      arg :password, non_null(:string), description: "user password"
      resolve &KaravanWeb.Resolvers.User.register_user/3
      middleware &build_payload/2
    end
  end
end
