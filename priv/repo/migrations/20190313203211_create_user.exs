defmodule Karavan.Repo.Migrations.CreateUser do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :email, :string, null: false
      add :password_hash, :string
      add :confirmed_at, :utc_datetime
      add :confirmation_sent_at, :utc_datetime
      add :confirmation_token, :string

      timestamps()
    end

    create unique_index(:users, [:email])

  end
end
