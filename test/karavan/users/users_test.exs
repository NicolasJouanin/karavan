defmodule Karavan.UsersTest do
  use Karavan.DataCase

  alias Karavan.Users
  alias Karavan.Users.User

  describe "users" do
    @valid_attrs %{email: "foo@bar.tld", password: "some password"}
    @invalid_password_attrs %{email: "foo@bar.tld", password: "pass"}
    test "register/1 with valid data creates a user" do
      assert {:ok, %User{email: email} = user} = Users.create(@valid_attrs)
      assert email == @valid_attrs.email
    end

    test "register/1 with invalid password returns error" do
      assert {:error, changeset} = Users.create(@invalid_password_attrs)
    end
  end
end
