defmodule KaravanWeb.Schema.UserTest do
  use KaravanWeb.ConnCase
  import KaravanWeb.GraphqlHelper

  describe "user mutations" do
    @query """
    mutation RegisterUser($email: String!, $password: String!) {
      registerUser(email: $email, password: $password) {
        successful
        messages { message }
        result {
          email
          confirmation_token
       }
      }
    }
    """

    test "registration succeed with proper arguments" do
      res = build_conn() |> graphql_query(query: @query, variables: %{
        email: "test@domain.tld",
        password: "password"
      })
      assert %{
        "data" => %{
          "registerUser" => %{
            "successful" => true,
            "messages" => messages,
            "result" => result
          }
        }
      } = res
      assert length(messages) == 0
    end

    test "registration fails with invalid email" do
      res = build_conn() |> graphql_query(query: @query, variables: %{
        email: "invalid_email@",
        password: "password"
      })
      assert %{
        "data" => %{
          "registerUser" => %{
            "successful" => false,
            "messages" => messages,
            "result" => result
          }
        }
      } = res
      assert length(messages) > 0
    end
  end
end
