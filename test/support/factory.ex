defmodule Karavan.Factory do
  @moduledoc """
  ExMachina factory for fixtures
  """

  use ExMachina.Ecto, repo: Karavan.Repo

  def user_factory() do
    %Karavan.Users.User{
      password_hash: "123456789",
      email: sequence(:email, &"email-#{&1}@domain.tld"),
      confirmed_at: DateTime.utc_now() |> DateTime.truncate(:second),
      confirmation_sent_at: nil,
      confirmation_token: nil
    }
  end
end